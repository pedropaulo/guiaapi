#!/usr/bin/python
# -*- coding: utf-8 -*-

################ Bibliotecas utilizadas ##########################
from flask import Flask, request, session, g, Markup, redirect, url_for, abort, \
	 render_template, flash, jsonify, escape
from flask_sqlalchemy import SQLAlchemy
#from flask.ext.bcrypt import Bcrypt
import bcrypt
from unicodedata import normalize
import random
import json
import socket
import time
import datetime
import netifaces as ni
import sys
import enum
import os
# from pml import app
#import requests
#import secrets

###################################################################

app = Flask(__name__)
#bcrypt = Bcrypt(app)
app.config.from_object(__name__)

app.config['DEBUG'] = True

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///guia.sqlite3'

app.config['SECRET_KEY'] = "random string"

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True

db = SQLAlchemy(app)

####################################################################
####################################################################
##############################Banco de dados##########################

class TipoRoteiro(enum.Enum):
    AVNTURA_ESPORTE = "Aventura/Esporte"
    CULINARIO = "Culinario"
    ECOTURISMO = "Ecoturismo"
    HISTORICO = "Historico"
    PESCA = "Pesca"
    TURISMO_RELIGIOSO = "Turismo Religioso"
    COMPRAS = "Compras"
    LAGOAS_CACHOEIRAS = "Lagoas e Cachoeiras"
    VIDA_NOTURNA = "Vida Noturna"
    PRAIA_ESPORTE_AQUATICO = "Praia e esportes aquaticos"

class Pessoa(db.Model):
	id_ = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(50))
	email = db.Column(db.String(50))
	password = db.Column(db.String(50))
	rg = db.Column(db.String(50))
	cpf = db.Column(db.String(50))
	telefone = db.Column(db.String(50))
	# pessoa = db.relationship('Pessoa', backref='pessoa', lazy='dynamic')

class Guia(db.Model):
	id_ = db.Column(db.Integer, primary_key=True)
	media_estrelas = db.Column(db.String(50))
	endereco = db.Column(db.String(50))
	img_rg = db.Column(db.String(50))
	img_cpf = db.Column(db.String(50))
	antecedentes = db.Column(db.String(50))
	curriculo = db.Column(db.String(50))
	img_residencia = db.Column(db.String(50))
	nivel_ingles = db.Column(db.String(50))
	cadastur = db.Column(db.String(50))
	conducao = db.Column(db.String(50))
	id_pessoa = db.Column(db.Integer)
	# id_pessoa = db.Column(db.Integer, db.ForeignKey('pessoa.id_'))
	# guia = db.relationship('Guia', backref='guia', lazy='dynamic')

class Turista(db.Model):
	id_ = db.Column(db.Integer, primary_key=True)
	pontos = db.Column(db.String(50))
	turistacol = db.Column(db.String(50))
	id_pessoa = db.Column(db.Integer)
	# id_pessoa = db.Column(db.Integer, db.ForeignKey('pessoa.id_'))
	# turista = db.relationship('Turista', backref='turista', lazy='dynamic')

class Roteiros(db.Model):
	id_ = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(50))
	descricao = db.Column(db.String(50))
	img_roteiro = db.Column(db.String(50))
	# fruit_type = db.Column(db.Enum(TipoRoteiro))
	# roteiros = db.relationship('Roteiros', backref='roteiros', lazy='dynamic')

class Passeio(db.Model):
	id_ = db.Column(db.Integer, primary_key=True)
	nome = db.Column(db.String(50))
	valor = db.Column(db.String(50))
	media_estrelas_passeio = db.Column(db.String(50))
	# id_guia = db.Column(db.Integer, db.ForeignKey('guia.id_'))
	id_guia = db.Column(db.Integer)
	id_roteiros = db.Column(db.Integer)
	# id_roteiros = db.Column(db.Integer, db.ForeignKey('roteiros.id_'))
	# passeio = db.relationship('Passeio', backref='passeio', lazy='dynamic')

class Feedback(db.Model):
	id_ = db.Column(db.Integer, primary_key=True)
	comentario = db.Column(db.String(50))
	estrelas = db.Column(db.String(50))
	id_guia = db.Column(db.Integer)
	# id_guia = db.Column(db.Integer, db.ForeignKey('guia.id_'))
	id_roteiros = db.Column(db.Integer)
	# id_roteiros = db.Column(db.Integer, db.ForeignKey('roteiros.id_'))
	id_roteiros = db.Column(db.Integer)
	# id_roteiros = db.Column(db.Integer, db.ForeignKey('passeio.id_'))

####################################################################
##############################Login######################################

@app.route('/',  methods=['GET'])
def home():
	print("Olá! você por aqui?")

@app.route('/api/turista/login',  methods=['POST'])
def login():
	if request.method == 'POST':
		email 	 = request.form['email']
		password = request.form['password']
		pessoa = Pessoa.query.filter_by(email=email).first()
		if pessoa is None:
			response = 'erro'
		else:
			if pessoa.email == email and pessoa.password == password:
				turista = Turista.query.filter_by(id_pessoa=pessoa.id_).first()
				response = jsonify(pessoa_email = pessoa.email,
									pessoa_name = pessoa.name,
									pontos = turista.pontos
									)

			else:
				response = 'erro'

	else:
		response = '404'
	return response

##################################################################
##################Index########################################

@app.route('/')
def index():
	return render_template('index.html')

#####################################################################################################################
###############################Cadastro de Turista###################################################################


@app.route('/cadastro', methods = ['POST', 'GET'])
def cadastro():
	print(request.data)
	if request.method == 'POST':
		name=request.form['nome']
		email=request.form['email']
		password=request.form['password']
		rg=request.form['rg']
		cpf=request.form['cpf']
		telefone=request.form['telefone']

		if not name or not email or not password:
			return jsonify(
							error = 'Campo vazio!'
				)
		else:
			pessoa_2 = Pessoa.query.filter_by(email=email).first()
			if pessoa_2 is None:
				pessoa = Pessoa(name=name,email=email,password=password,rg=rg,cpf=cpf,telefone=telefone)
				db.session.add(pessoa)
				db.session.commit()
				pessoa_id = pessoa.id_
				
				turista = Turista(id_pessoa = pessoa_id)
				db.session.add(turista)
				db.session.commit()

				return jsonify(
							msg = 'Cadastrado com sucesso!'
				)

			else:
				return jsonify(
							error = 'Pessoa já possui cadastro!'
				)
	else:
		return jsonify(
							error = 'Não possui método POST esperado!'
				)

#################################################################################
###############################Register##########################################


@app.route('/register', methods=['POST', 'GET'])
def register():

	if request.method == 'POST':
		name      = request.form['name']
		email     = request.form['email']
		password  = request.form['password']
		if not name or not email or not password:
			message = Markup("<div class='col alert alert-success alert-dismissible fade show' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button><h4 class='alert-heading'>Os campos nao podem ser enviados vazios!</h4><p></p></div>")
			flash(message)
			return render_template('index.html')
		else:
			user = Users.query.filter_by(email=email).first()
			if user is None:
				user = Users(name=name,email=email,password=password)
				db.session.add(user)
				db.session.commit()
				message = Markup("<div class='col alert alert-success alert-dismissible fade show' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button><h4 class='alert-heading'>Cadastrado com sucesso!</h4><p></p></div>")
			else:
				message = Markup("<div class='col alert alert-success alert-dismissible fade show' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button><h4 class='alert-heading'>Usuário ja cadastrado!</h4><p></p></div>")
			
			flash(message)
			return render_template('index.html')
	else:
		return render_template('index.html')

###################################################################################################################
####################################Update#########################################################################
@app.route('/api/save/update', methods=['POST', 'GET', 'PUT'])
def update():
	if request.method == 'POST':
		save_update = request.form['save_update'];
		# jsonUpdate = request.get_json()
		#jsonUpdate = '{"token": "123456","date": "2018−02−28T20:29:16.331Z","inventory": [4 , 6 , 2 , 1 ],"places": [{"id" : 1 ,"isUnlocked" : true ,"isVisited" : true ,"item" : [1 ,4]} ,{"id" : 3 ,"isUnlocked" : true ,"isVisited" : false ,"items" : [3 , 5]}]}'
		save_update = json.loads(save_update)

		token = save_update['token']
		itens = save_update['itens']
		places = save_update['places']
		save_date = datetime.datetime.now()
		user = Users.query.filter_by(token=token).first()
		if user:
			# user.token = genarateToken(user.id_)
			new_save = Saves(datetime=save_date, itens=itens, id_users=user.id_)
			db.session.add(new_save)
			db.session.commit()
			id_ = new_save.id_
			for place in places:
				new_place = Places(id_=place['id'],isUnlocked = place['isUnlocked'], isVisited =  place['isVisited'],itens=place['itens'], id_saves=id_)
				db.session.add(new_place)
				db.session.commit()
			save = structureSave(user.id_, user.token)
			response = jsonify(save)
		else:
			response = 'erro'
	else:
		response = 'erro'
	return response
@app.route('/api/save/autentication', methods=['POST'])
def autenticate():
	if request.method == 'POST':
		token = request.form['token']
		datetime = request.form['datetime']
		user = Users.query.filter_by(token=token).first()
		if user is None:
			response = 'erro'
		else:
			user.token = genarateToken(user.id_)
			db.session.add(user)
			db.session.commit()
			save = structureSave(user.id_, user.token)
			response = jsonify(save)
	else:
		response = '404'
	return response

def genarateToken(user_id):
	hash = 'macacos me mordam'
	pw_hash = bcrypt.hashpw(hash.encode('utf8'), bcrypt.gensalt())
	candidate = str(pw_hash) + str(random.random() + user_id) + str(random.random()) + str(user_id)
	token = str(bcrypt.hashpw(candidate.encode('utf8'), bcrypt.gensalt()))
	#return hash
	return  normalize('NFKD', token).encode('ASCII', 'ignore').decode('ASCII')


###################################################################################################################
###################################################################################################################
###################################################################################################################
###################################################################################################################
###################################################################################################################
###################################################################################################################
app.secret_key = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'
if __name__ == '__main__':
	#app.run(debug = True)
	db.create_all()
	db.session.commit()
	#cmd = "ifconfig eth0 | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}'"
	#p = Popen(cmd, shell=True, stdout=PIPE, stderr=PIPE)
	#Recebendo o IP que está na placa no eth0
	#AdressIP, err = p.communicate()
	#AdressIP = socket.gethostbyname(socket.gethostname())
	# reload(sys)
	# sys.setdefaultencoding('utf8')
	# ni.ifaddresses('eth0')
	# ip = ni.ifaddresses('eth0')[ni.AF_INET][0]['addr']
	# app.run(host=ip, port=5000, debug=True, threaded=True)
	port = int(os.environ.get("PORT", 5000))
	app.run(host='0.0.0.0', port=port)
